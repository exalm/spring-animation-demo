// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/property-row.ui")]
public class Elastic.PropertyRow : Adw.Bin {
    [GtkChild]
    private unowned Gtk.Scale scale;

    public string title { get; set; }
    public string description { get; set; }
    public string description_tooltip { get; set; }
    public uint digits { get; set; }
    public double upper { get; set; }
    public double lower { get; set; }
    public double value { get; set; }
    public double step_increment { get; set; }
    public double page_increment { get; set; }

    public double mark { get; set; }
    public double secondary_mark { get; set; }
    public bool has_secondary_mark { get; set; }

    construct {
        update_marks ();

        notify["mark"].connect (update_marks);
        notify["secondary-mark"].connect (update_marks);
        notify["has-secondary-mark"].connect (update_marks);
    }

    private void update_marks () {
        scale.clear_marks ();

        scale.add_mark (mark, BOTTOM, null);

        if (has_secondary_mark)
            scale.add_mark (secondary_mark, BOTTOM, null);
    }

    static construct {
        set_css_name ("property-row");
    }
}
