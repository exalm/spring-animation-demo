// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/interactive-view.ui")]
public class Elastic.InteractiveView : TransformBin {
    private SpringParams _spring;
    public SpringParams spring {
        get { return _spring; }
        set {
            if (spring == value)
                return;

            if (spring != null)
                spring.changed.disconnect (reset_animation);

            _spring = value;

            if (spring != null)
                spring.changed.connect (reset_animation);

            reset_animation ();
        }
    }

    [GtkChild]
    private unowned Gtk.Gesture drag_gesture;
    [GtkChild]
    private unowned Gtk.Gesture swipe_gesture;

    private Adw.SpringAnimation animation_x;
    private Adw.SpringAnimation animation_y;

    private double start_x;
    private double start_y;
    private double last_x;
    private double last_y;

    static construct {
        set_css_name ("interactive-view");
    }

    [GtkCallback]
    private void drag_begin_cb (double start_x, double start_y) {
        if (pick (start_x, start_y, Gtk.PickFlags.DEFAULT) != child) {
            drag_gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        drag_gesture.set_state (Gtk.EventSequenceState.CLAIMED);
        this.start_x = last_x;
        this.start_y = last_y;

        animation_x.reset ();
        animation_y.reset ();

        set_translation (this.start_x, this.start_y);

        child.cursor = new Gdk.Cursor.from_name ("grabbing", null);
    }

    [GtkCallback]
    private void drag_update_cb (double offset_x, double offset_y) {
        set_translation (offset_x + start_x, offset_y + start_y);
    }

    [GtkCallback]
    private void drag_cancel_cb () {
        animate (0, 0);
    }

    construct {
        child.cursor = new Gdk.Cursor.from_name ("grab", null);

        swipe_gesture.group (drag_gesture);

        animation_x = new Adw.SpringAnimation (
            child, 0, 0, new Adw.SpringParams.full (1, 1, 1),
            new Adw.CallbackAnimationTarget (value => {
                set_translation (animation_x.value, last_y);
            })
        );

        animation_y = new Adw.SpringAnimation (
            child, 0, 0, new Adw.SpringParams.full (1, 1, 1),
            new Adw.CallbackAnimationTarget (value => {
                set_translation (last_y, animation_y.value);
            })
        );

        animation_x.follow_enable_animations_setting = false;
        animation_y.follow_enable_animations_setting = false;
    }

    [GtkCallback]
    private void animate (double velocity_x, double velocity_y) {
        child.cursor = new Gdk.Cursor.from_name ("grab", null);

        spring.apply (animation_x);
        spring.apply (animation_y);

        animation_x.value_from = last_x;
        animation_y.value_from = last_y;

        animation_x.initial_velocity = velocity_x;
        animation_y.initial_velocity = velocity_y;

        animation_x.play ();
        animation_y.play ();
    }

    private void set_translation (double x, double y) {
        last_x = x;
        last_y = y;
        transform = new Gsk.Transform ().translate ({ (float) x, (float) y });
    }

    private void reset_animation () {
        animation_x.reset ();
        animation_y.reset ();

        set_translation (0, 0);
    }

    private bool is_child_out_of_bounds () {
        double width = get_width ();
        double height = get_height ();
        double child_width = child.get_width ();
        double child_height = child.get_height ();

        if (last_x < (width - child_width) / -2)
            return true;
        if (last_x > (width - child_width) / 2)
            return true;
        if (last_y < (height - child_height) / -2)
            return true;
        if (last_y > (height - child_height) / 2)
            return true;

        return false;
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        if (!is_child_out_of_bounds ())
            snapshot_child (child, snapshot);
    }

    protected override void unmap () {
        base.unmap ();

        reset_animation ();
    }

    public void snapshot_toplevel (Gtk.Widget toplevel, Gtk.Snapshot snapshot) {
        Graphene.Matrix matrix;

        if (!is_child_out_of_bounds ())
            return;

        if (!compute_transform (toplevel, out matrix))
            return;

        snapshot.save ();
        snapshot.transform_matrix (matrix);

        snapshot_child (child, snapshot);

        snapshot.restore ();
    }
}
